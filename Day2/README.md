# APIN Traning day 2 - Linux & Software Developments

## Linux exercises

Explain below CMDs: learn by heart…

```shell
1. tail -1000f /var/log/messages
2. ps -ef |grep cf
3. find /home/log/ -type f -exec du -Sh {} + | sort -rh | head -n 5
4. du -Sh /home/ | sort -rh | head -5
5. ls -ltr Week*.html | tail -1 | awk -F "_" '{print $2}' | awk -F "." '{print $1}'
6. grep -a "ID :" /home/omni/bin/ibcf | grep ibcf
7. yum list installed | grep cf
8. free –h
9. echo 3 > /proc/sys/vm/drop_caches
10. scp /home/omni/bin/dbcache64 omni@192.168.33.122:/home/omni/bin/dbcache64;
11. sed -i "s/ipc2netcln64t/ipc2netclnt64/g" *.txt
12. chmod +755 whilelabel.sh
13. chown omni:exploit /home/omni/bin/pcscf
```

1. `tail -1000f /var/log/messages`
    In ra 1000 dòng cuối file `messages` và cập nhật trong thời gian thực (tuỳ chọn `-f`).

2. `ps -ef |grep cf`

    - `ps`: Hiển thị các process (tiến trình / chương trình) đang được vận hành trong hệ thống Linux. Các tuỳ chon `-e`: Hiển thị tất cả tiến trình; `-f`: Liệt kê theo dạng đầy đủ thông tin như UID, PPOD, TIME, ...
    - `grep`: Tìm chuỗi trong file chỉ định
    - Kết quả của `ps -ef` là đầu vào cho lệnh tìm kiếm `grep` tiến trình có tên `cf`.

3. `find /home/log/ -type f -exec du -Sh {} + | sort -rh | head -n 5`

    - `find`: Tìm kiếm file/folder.
    - `sort`: Sắp xếp
    - `head`: Hiển thị một vài dòng đầu tiên của file
    - `du -Sh`
    - Tìm kiếm tất cả file trong `/home/log`, tiến hành chạy lệnh `du` để tìm thông tin size của các file đó, sau đó sort theo reverse order và in ra 5 file log đó.

4. `du -Sh /home/ | sort -rh | head -5`
    In ra thông tin size của tất cả file, folder trong `/home/` (Bao gồm cả các sub dirs), sau đó sắp xếp theo reverse order (by size) và in ra 5 dir đầu tiên.

5. `ls -ltr Week*.html | tail -1 | awk -F "_" '{print $2}' | awk -F "." '{print $1}`

6. `grep -a "ID :" /home/omni/bin/ibcf | grep ibcf`
    Tìm `"ID :"` trong file ibcf (option a process với file dưới dạng binary), sau đó tìm `ibcf`  trong kết quả vừa tìm được.

7. `yum list installed | grep cf`
    List toàn bộ packages đã được cài trên hệ thống và tìm package tên **cf**.

8. `free –h`
    Xem thông tin tài nguyên của hệ thống (dưới dạng người đọc -h option).

9. `echo 3 > /proc/sys/vm/drop_caches`
    In `3` vào trong file `/proc/sys/vm/drop_caches`.

10. `scp /home/omni/bin/dbcache64 omni@192.168.33.122:/home/omni/bin/dbcache64`
    Secure copy file dbcache64 đến server.

11. `sed -i "s/ipc2netcln64t/ipc2netclnt64/g" *.txt` 
    Replace tất cả từ `ipc2netcln64t` bằng `ipc2netcln64` trong tất cả file txt.

12. `chmod +755 whilelabel.sh`
    Cấp quyền 755 cho file.

13. `chown omni:exploit /home/omni/bin/pcscf`
    Đổi group owner của pcscf thành exploit.